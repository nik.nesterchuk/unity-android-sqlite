﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using SQLite4Unity3d;
using UniRx.Async;

public class DatabaseAdapter
{
    //---------------------------------------------------------------------
    // Internal
    //---------------------------------------------------------------------

    private SQLiteConnection _connection;
    
    //---------------------------------------------------------------------
    // Properties
    //---------------------------------------------------------------------
    
    public bool IsInitialized { get; private set; }

    //---------------------------------------------------------------------
    // Public
    //---------------------------------------------------------------------

    public async void Init(string databaseName)
    {
        var path = Path.Combine(Application.persistentDataPath, databaseName);
        if (!File.Exists(path))
        {
            await CopyDatabaseFromStreamingAssets(databaseName, path);
        }
        _connection = new SQLiteConnection(path, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
        IsInitialized = true;
        Debug.Log($"DATABASE PATH: {path}");
    }

    public IEnumerable<T> GetAllData<T>() where T : new()
    {
        if (_connection == null)
        {
            throw new InvalidOperationException("Adapter is not initialized");
        }

        return _connection.Table<T>();
    }

    public void WriteEntry<T>(T model) where T : new()
    {
        if (_connection == null)
        {
            throw new InvalidOperationException("Adapter is not initialized");
        }
        
        _connection.Insert(model);
    }

    public void Dispose()
    {
        _connection.Close();
        _connection = null;
    }

    //---------------------------------------------------------------------
    // Helpers
    //---------------------------------------------------------------------

    private static async UniTask CopyDatabaseFromStreamingAssets(string databaseName, string targetPath)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        var streamingAssetsPath = $"jar:file://{Application.dataPath}!/assets/{databaseName}";
        var data = await UnityWebRequest.Get(streamingAssetsPath).SendWebRequest();
        File.WriteAllBytes(targetPath, data.downloadHandler.data);
#else
        var streamingAssetsPath = Path.Combine(Application.dataPath, "StreamingAssets", databaseName);
        File.Copy(streamingAssetsPath, targetPath);
#endif
        Debug.Log("Successfully copied database!");
    }
}