﻿using UnityEngine;
using UniRx.Async;

public class Main : MonoBehaviour
{
    //---------------------------------------------------------------------
    // Editor
    //---------------------------------------------------------------------

    [Header("Settings")]
    [SerializeField] private string workersDatabaseName;
    
    //---------------------------------------------------------------------
    // Internal
    //---------------------------------------------------------------------

    private DatabaseAdapter _adapter;
    
    //---------------------------------------------------------------------
    // Messages
    //---------------------------------------------------------------------

    private async void Start()
    {
        _adapter = new DatabaseAdapter();
        _adapter.Init(workersDatabaseName);
        await UniTask.WaitUntil(() => _adapter.IsInitialized);
        PrintAllWorkers();
        Debug.Log("----------------------");
        _adapter.WriteEntry(new Worker{Age = 34, Name = "Vitalik"});
        PrintAllWorkers();
    }

    private void OnDestroy()
    {
        _adapter.Dispose();
    }

    //---------------------------------------------------------------------
    // Helpers
    //---------------------------------------------------------------------

    private void PrintAllWorkers()
    {
        var workers = _adapter.GetAllData<Worker>();
        foreach (var worker in workers)
        {
            Debug.Log(worker);
        }
    }
}